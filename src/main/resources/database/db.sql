create database tasklist character set utf8 collate utf8_general_ci;

-- we don't know how to generate root <with-no-name> (class Root) :(
create table category
(
    id bigint auto_increment
        primary key,
    title varchar(45) not null,
    completed_count bigint null,
    uncompleted_count bigint null
);

create table priority
(
    id bigint auto_increment
        primary key,
    title varchar(45) null,
    color varchar(45) null
);

create table stat
(
    id bigint auto_increment
        primary key,
    completed_total_count bigint null,
    uncompleted_total_count bigint null
);

create table task
(
    id bigint auto_increment
        primary key,
    title varchar(45) not null,
    completed int default 0 null,
    date datetime null,
    priority_id bigint null,
    category_id bigint null,
    constraint fk_category_idx
        foreign key (category_id) references category (id)
            on delete set null,
    constraint fk_priority_idx
        foreign key (priority_id) references priority (id)
            on delete set null
);

create index fk_category_idx_idx
    on task (category_id);

create index fk_priority_idx_idx
    on task (priority_id);

