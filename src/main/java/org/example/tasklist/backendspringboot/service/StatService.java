package org.example.tasklist.backendspringboot.service;

import org.example.tasklist.backendspringboot.entity.Stat;
import org.example.tasklist.backendspringboot.repo.StatRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class StatService {

    private final StatRepository statRepository;

    public StatService(StatRepository statRepository) {
        this.statRepository = statRepository;
    }

    public Stat findById(Long id){
        return statRepository.findById(id).get();
    }

}
